/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author p1601511
 */
public class Couloir extends Case {
    
    private boolean pacGomme;
    private boolean superPacGomme;

    public Couloir() {
        this.pacGomme = false;
        this.superPacGomme = false;
    }
    
    public boolean isPacGomme() {
        return pacGomme;
    }

    public boolean isSuperPacGomme() {
        return superPacGomme;
    }

    public void setPacGomme(boolean pacGomme) {
        this.pacGomme = pacGomme;
    }

    public void setSuperPacGomme(boolean superPacGomme) {
        this.superPacGomme = superPacGomme;
    }
    
    
    
}
