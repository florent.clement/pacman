/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author p1601511
 */
public abstract class Entite implements Runnable {
  
    /**
     * Vie de l'entité ( 1 pour un fantôme et 3 pour le pacman )
     */
    protected Integer vie;
    
    /**
     * Jeu sur lequel l'entité est présente
     */
    protected Jeu jeu;       
            
    /**
     * Direction actuel de l'entité
     */
    protected Direction currentDirection;
    
    public Entite(Jeu jeu)
    {
        this.jeu = jeu;
    }
    
    /**
     * Fonction permettant de déplacer l'entité
     */
    protected abstract void realiserAction();
    
    /**
     * Décrémente la vie de l'entité
     */
    public void kill(){
        this.vie --;
    }
    
    public Direction getDirection(){
        return this.currentDirection;
    }
    
    public abstract void setDirection(Direction direction);

    public Integer getVie() {
        return vie;
    }

    public Jeu getJeu() {
        return jeu;
    }
    
    @Override
    public void run() 
    {
        /*
            tant que la partie / niveau n'est pas terminé ou que l'entité n'est
            morte on continue
        */
        while(!jeu.finPartie() && this.vie != 0)
        {
            synchronized(this)
            {
                try {
                    // attend que le Thread de gestion d'IA le notify
                    this.wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Entite.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.realiserAction();
            }
        }
    }
    
    /**
     * retourne la liste des directions que peut prendre l'entité
     * @return List<Direction> 
     */
    public List<Direction> getDirectionPossible(){
        return jeu.getDirectionPossible(this);
    }

}