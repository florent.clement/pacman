/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import outils.IA;

/**
 *
 * @author p1601511
 */
public class Fantome extends Entite {

    public Fantome(Jeu jeu) {
        super(jeu);
        super.vie = 1;
    }

    @Override
    protected void realiserAction() {
        this.jeu.deplacer(this, IA.Astar(this.jeu.getPlateau(), this.jeu.getPacman(), this));
    }

    @Override
    public void setDirection(Direction direction) {
        super.currentDirection = direction;
    }
}