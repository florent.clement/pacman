/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import outils.IA;
import outils.TetrisBuild;
import outils.Tool;

/**
 *
 * @author p1601511
 */
public class Jeu extends Observable {

    private int maxLength;
    private int maxWidth;
    private int nbEntite;
    private int superPacGomme;
    private Case[][] plateau;
    private Map<Entite, Position> tabEntite;
    private Entite pacman;
    public Thread threadIA;
    private SablierSuperPacGomme sablierSuperPacGomme;
    private Integer point;
    private TetrisBuild tetrisBuild;
    private Integer nbPacGomme;
    private boolean isClassicMaze = true;

    public Jeu() {
        this.maxLength = 28;
        this.maxWidth = 30;
        this.nbEntite = 4;
        this.point = 0;
        this.superPacGomme = 3;
        this.tabEntite = new HashMap<>();
        this.sablierSuperPacGomme = new SablierSuperPacGomme();
        this.tetrisBuild = new TetrisBuild(this.maxLength, this.maxWidth, this.superPacGomme);
    }

    public void setIsClassicMaze(boolean isClassicMaze) {
        this.isClassicMaze = isClassicMaze;
    }

    public Integer getPoint() {
        return point;
    }

    public void initialiser() {

        if (isClassicMaze) {
            this.plateau = tetrisBuild.build();
        } else {
            this.plateau = Tool.generateMaze(maxLength, maxWidth);
        }

        this.tabEntite = IA.generateEntite(this, nbEntite);

        if (this.threadIA != null) {
            this.threadIA.interrupt();
        }
        this.threadIA = new Thread(new IA(this));
        this.threadIA.start();
        this.nbPacGomme = 0;
        for (int i = 0; i < this.maxLength; i++) {
            for (int j = 0; j < this.maxWidth; j++) {
                if (this.getCase(i, j) instanceof Couloir) {
                    if (((Couloir) this.getCase(i, j)).isPacGomme()
                            || ((Couloir) this.getCase(i, j)).isSuperPacGomme()) {
                        this.nbPacGomme++;
                    }
                }
            }
        }
    }

    public void reinitialiser() {
        this.tabEntite.clear();
        this.initialiser();
    }

    public Entite getPacman() {
        return pacman;
    }

    public void setPacman(Entite pacman) {
        this.pacman = pacman;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public boolean finPartie() {
        int entiteAlive = 0;
        entiteAlive = this.tabEntite.keySet().stream().filter((entite) -> (entite.getVie() > 0 && entite instanceof Fantome)).map((_item) -> 1).reduce(entiteAlive, Integer::sum);
        return this.getPacman().getVie() == 0 || this.nbPacGomme == 0 || entiteAlive == 0;
    }

    public boolean directionEstPossible(Entite entite, Direction direction) {
        return this.getCaseWithDpl(this.tabEntite.get(entite), direction) instanceof Couloir;
    }

    /**
     * Fonction pour déplacer une entité sur le plateau
     *
     * @param entite Entite ciblé
     * @param direction direction visé
     */
    public synchronized void deplacer(Entite entite, Direction direction) {
        Position position = this.tabEntite.get(entite);
        
        if (entite instanceof PacMan)
            this.pacmanKillEntite(position);
        
        Case caseDpl = this.getCaseWithDpl(position, direction);

        if (caseDpl instanceof Couloir) {

            position.setPositionWithDirection(direction);
            if (position.getX() == 0) {
                position.setX(maxLength - 1);
            } else if (position.getX() == maxLength - 1) {
                position.setX(0);
            }

            if (entite instanceof PacMan) {
                this.point += this.calcPoint(caseDpl);
                if (((Couloir) caseDpl).isSuperPacGomme()) {
                    if (this.sablierSuperPacGomme.isActive()) {
                        this.sablierSuperPacGomme.restart();
                    } else if (!this.sablierSuperPacGomme.getThreadSablier().isAlive()) {
                        this.sablierSuperPacGomme.start();
                    }
                }
                this.pacmanKillEntite(position);
            }

            //Notification de la vue, suite à la mise à jour du champ lastValue
            setChanged();
            //On renvoie l'entité afin de pouvoir déterminer laquelle à bouger et la rafraichir dans la vue
            notifyObservers(entite);
        }

    }

    private Integer calcPoint(Case src) {
        Integer pointCalc = 0;

        // pacgomme et super pacgomme
        if (src instanceof Couloir) {
            Couloir couloir = (Couloir) src;
            if (couloir.isPacGomme()) {
                pointCalc += 10;
            } else if (couloir.isSuperPacGomme()) {
                pointCalc += 50;
            } else {
                pointCalc += 0;
            }
        }
        return pointCalc;
    }

    public Case[][] getPlateau() {
        return plateau;
    }

    public Case getCase(int x, int y) {
        return plateau[x][y];
    }

    public Case getCase(Position position) {
        return this.getCase(position.getX(), position.getY());
    }

    public Case getCaseWithDpl(Position position, Direction direction) {
        Position pos = position.clone();
        pos.setPositionWithDirection(direction);
        if (pos.getX() < 0 || pos.getX() >= this.maxLength
                || pos.getY() < 0 || pos.getY() >= this.maxWidth) {
            return null;
        } else {
            return this.getCase(pos.getX(), pos.getY());
        }
    }

    public Map<Entite, Position> getTabEntite() {
        return tabEntite;
    }

    public List<Direction> getDirectionPossible(Entite entite) {
        return this.getDirectionPossible(this.tabEntite.get(entite));
    }

    public List<Direction> getDirectionPossible(Position position) {
        List<Direction> lDirection = new ArrayList<>();
        for (Direction direction : Direction.values()) {
            if (this.getCaseWithDpl(position, direction) != null
                    && direction != direction.AUCUNE) {
                if (this.getCase(position.getX(), position.getY()) instanceof Couloir) {
                        lDirection.add(direction);
                }
            }
        }
        return lDirection;
    }

    public SablierSuperPacGomme getSablierSuperPacGomme() {
        return sablierSuperPacGomme;
    }

    public void decrementePacGomme() {
        this.nbPacGomme--;
    }

    public void nextLevel() {
        if (this.getPacman().getVie() == 0) {
            this.point = 0;
            ((PacMan) this.getPacman()).reborn();
        }
        this.initialiser();
        this.sablierSuperPacGomme.stop();
    }

    public void nextRound() {

        // pacgomme
        Couloir couloir = (Couloir) this.getCase(this.getTabEntite().get(this.getPacman()));
        if (couloir.isPacGomme() || couloir.isSuperPacGomme()) {
            if (couloir.isSuperPacGomme()) {
                this.getSablierSuperPacGomme().restart();
            }
        }

        this.setChanged();
        this.notifyObservers(this);
    }

    public ArrayList<Position> getCouloir() {
        ArrayList<Position> listeCouloirPosition = new ArrayList<>();
        for (int i = 0; i < this.getMaxLength(); i++) {
            for (int j = 0; j < this.getMaxWidth(); j++) {
                if (this.getCase(i, j) instanceof Couloir) {
                    listeCouloirPosition.add(new Position(i, j));
                }
            }
        }
        return listeCouloirPosition;
    }

    public void defineNewPositionForPacman() {
        ArrayList<Position> lCouloir = this.getCouloir();
        this.tabEntite.replace(this.getPacman(), lCouloir.get(Tool.monRandom(0, lCouloir.size())));
    }

    private void pacmanKillEntite(Position positionPacman) {
        Couloir couloir = (Couloir) this.getCase(positionPacman);
        if (couloir.isPacGomme() || couloir.isSuperPacGomme()) {
            this.decrementePacGomme();
            if (couloir.isSuperPacGomme()) {
                this.getSablierSuperPacGomme().restart();
                couloir.setSuperPacGomme(false);
            } else {
                couloir.setPacGomme(false);
            }
        }

        List<Fantome> lEntite = new ArrayList<>();
        this.tabEntite.forEach((entite, position) -> {
            if (entite instanceof modele.Fantome && entite.getVie() != 0
                    && position.equals(positionPacman)) {
                lEntite.add((Fantome) entite);
            }
        });

        if (lEntite.size() > 0) {
            if (this.sablierSuperPacGomme.isActive()) {
                this.point += 200 + (this.sablierSuperPacGomme.getKillEntite() * 200);
                this.sablierSuperPacGomme.incrementKillEntite();
                lEntite.forEach((entite) -> {
                    entite.kill();
                });
            } else {
                this.getPacman().kill();
                this.defineNewPositionForPacman();
                this.notifyObservers(this.getPacman());
            }
        }
    }
}
