/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author p1601511
 */
public class PacMan extends Entite   {

    public PacMan(Jeu jeu) {
        super(jeu); 
        super.currentDirection = Direction.NORD;
        this.vie = 3;
    }
    
    @Override
    protected void realiserAction() {
         this.jeu.deplacer(this, currentDirection);
    }

    @Override
    public void setDirection(Direction direction) {
        if(this.jeu.directionEstPossible(this, direction))
            super.currentDirection = direction;
    }
    
    public void reborn(){
        this.vie = 3;
    }
}
