
package outils;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import modele.Case;
import modele.Couloir;
import modele.Direction;
import modele.Entite;
import modele.Jeu;
import modele.Position;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Florent
 */
public class IA implements Runnable {

    private Jeu jeu;
    
    private Map<Entite,Thread> tabThreadEntite;
    
    public IA(Jeu jeu) {
        this.jeu = jeu;
        this.tabThreadEntite = new HashMap<Entite,Thread>();
    }
    
    /**
     * 
     * @param entite
     * @return 
     */
    public static Direction basicDeplacement(Entite entite)
    {
        List<Direction> lDirectionPossible = entite.getDirectionPossible();
        
        if(lDirectionPossible.isEmpty())
            return Direction.AUCUNE;
        
        // cas avec 1 choix
        if(lDirectionPossible.size() == 1) 
            return lDirectionPossible.get(0);
        
        // CAS AVEC 2 CHOIX
        if(lDirectionPossible.size() == 1) {
            int indexCurrentDirection = 
                    entite.getDirection() == lDirectionPossible.get(0) ? 0 : 1;
            
            // On prend 20% de chance qu'il change de direction
            if(Tool.monRandom(1, 7) == 1)
                return lDirectionPossible.get( (indexCurrentDirection + 1) % 2 );
            else
                return lDirectionPossible.get( indexCurrentDirection );
        }
        
        
        // CAS AVEC 3 OU 4 CHOIX  
        return lDirectionPossible.get(Tool.monRandom(0, lDirectionPossible.size()));
    }
    
    private static ArrayList<Position> getPositionAdj(Position courant, Jeu jeu,
            ArrayList<Position> listFermee){
        
        ArrayList<Position> listPositionPossible = new ArrayList<>();
        Position positionPossible;
        for(Direction direction : jeu.getDirectionPossible(courant)){
            positionPossible = courant.clone();
            positionPossible.setPositionWithDirection(direction);
            listPositionPossible.add(positionPossible);
        }
        listPositionPossible.removeAll(listFermee);
        return listPositionPossible;
    }
    
    private static Position getMeilleurNoeud(ArrayList<Position> listPosition,Position arrive){
        if(listPosition.isEmpty())
            return null;
        
        Position meilleurNoeud = listPosition.get(0);
        for(Position position : listPosition){
            if(Tool.distance(position, arrive) < Tool.distance(meilleurNoeud, arrive))
                meilleurNoeud = position;
        }
        return meilleurNoeud;
    }
    
    public static Direction Astar(Case[][] plateau,Entite pacman,Entite entite)  {
        Jeu jeu = pacman.getJeu();
        Map<Entite,Position> tabEntite = jeu.getTabEntite();
        Position depart = tabEntite.get(entite);
        Position arrive;
        if(!jeu.getSablierSuperPacGomme().isActive()) {
             arrive = tabEntite.get(pacman);
        }
        else {
            arrive = Tool.mirrorPosition(tabEntite.get(pacman), jeu.getMaxLength(), jeu.getMaxWidth());
        }
        
        Position courant = depart.clone();
        
        if(depart.equals(arrive))
            return Direction.AUCUNE;
        
        // position possible
        ArrayList<Position> positionPossible = new ArrayList<>();
        positionPossible.add(courant);
        
        //positon fermé
        ArrayList<Position> positionFermee = new ArrayList<>();
        positionFermee.add(depart);
        
        // direction depart
        Map<Position,Position> mapRoute = new HashMap<>();
        
        while(!courant.equals(arrive) && !positionPossible.isEmpty())
        {
            // meilleur noeud
            courant = getMeilleurNoeud(positionPossible,arrive);
            positionFermee.add(courant);
            for(Position position : getPositionAdj(courant,jeu,positionFermee))
            {
                mapRoute.put(position, courant);
                if(positionPossible.stream().filter((pos) -> {
                    return pos.equals(position);
                }).count() == 0)
                    positionPossible.add(position);
            }
            positionPossible.remove(courant);
        }
        
        if(!courant.equals(arrive) || positionPossible.isEmpty())
            return basicDeplacement(entite);
        else{
            try{
                while(!mapRoute.get(courant).equals(depart)){
                    courant = mapRoute.get(courant);
                }
            } catch(java.lang.NullPointerException er){
                 return Direction.AUCUNE;
            }
        }
        return Direction.getDirection(depart, courant);
    }

    @Override
    public void run() {
       
        Boolean interrupted = false;
        
        for(Entite entite : this.jeu.getTabEntite().keySet())
        {
            Thread thread = new Thread(entite);
            this.tabThreadEntite.put(entite,thread);
            thread.start();
        }
        
        while(!this.jeu.finPartie() && !interrupted)
        {
            for( Entite entite : this.jeu.getTabEntite().keySet())
            {
                synchronized(entite)
                {
                    entite.notify();
                }
            }
            try {
                sleep(200);
            Platform.runLater(jeu::nextRound);
            } catch (InterruptedException ex) {
                interrupted = true;
            }
        }
    }
    
    /**
     * Génere par défaut un pacman + nb d'entite ( fantome )
     * @param jeu
     * @param nbEntite > 1
     * @return 
     */
    public static Map<Entite,Position> generateEntite(Jeu jeu, int nbEntite){
        // récuperation de la liste des couloirs
        ArrayList<Position> listeCouloirPosition = jeu.getCouloir();
        
        Map<Entite,Position> mapEntite = new HashMap<>();
        if(jeu.getPacman() == null)
            jeu.setPacman(new modele.PacMan(jeu));
        mapEntite.put(jeu.getPacman(), listeCouloirPosition.remove(Tool.monRandom(0, listeCouloirPosition.size())));
        
        // Génération des fantomes
        for(int i = 0; i < nbEntite; i++){
            mapEntite.put(new modele.Fantome(jeu), listeCouloirPosition.remove(Tool.monRandom(0, listeCouloirPosition.size())));
        }
        return mapEntite;
    }
}
