/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;

/**
 *
 * @author Florent
 */
public class ProgressBarBox extends HBox  {

    private Service threadProgressBar;
    
    private Integer pourcentage;
    
    public ProgressBarBox() {
        this.threadProgressBar = createThreadProgressBar();
        
        ProgressBar progressBar = new ProgressBar();
        progressBar.setMinSize(740, 30);
        progressBar.progressProperty().bind(threadProgressBar.progressProperty());
        this.pourcentage = 0;
        this.getChildren().add(progressBar);
       
        
    }
    
    /**
     * Création d'un service qui toutes les 10ms augmente de 1 jusqu'à 100
     * @return 
     */
    private Service createThreadProgressBar(){
        
        return new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws InterruptedException {
                        int pourcentage;
                        for(pourcentage = 0; pourcentage <= 100;pourcentage++)
                        {
                            updateProgress(pourcentage, 100);
                            Thread.sleep(10);
                        }
                        return pourcentage;
                    }
                    };
            }
        };
    }

    public Service getThreadProgressBar() {
        return threadProgressBar;
    }
    
    
    
    
    
    
}
