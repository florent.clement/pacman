/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

import java.util.ArrayList;
import modele.Case;
import modele.Couloir;
import modele.Direction;
import modele.Mur;
import modele.Position;

/**
 *
 * @author p1601511
 */
public class Tool {

    
    /**
     * Génère un nombre aléatoire entre min ( inclus ) et max ( exclus )
     * @param min
     * @param max
     * @return 
     */
    public static int monRandom(int min, int max) {
        return (int) (min + (Math.random() * (max - min)));
    }

    /**
     * Calcule de la distance entre deux points x et y
     * @param position1
     * @param position2
     * @return 
     */
    public static double distance(Position position1, Position position2) {
        return Math.abs(position2.getX() - position1.getX())
                + Math.abs(position2.getY() - position1.getY());
    }

    public static Case[][] generateMaze(int maxLength, int maxWidth) {
        int longMinCouloir = 2, longMaxCouloir = 6;
        int nbSuperPacgum = 3;
        //Remplissage du demi-tableau par des murs
        Case[][] plateauDemi = new Case[maxLength / 2][maxWidth];
        for (int i = 0; i < maxLength / 2; i++) {
            for (int j = 0; j < maxWidth; j++) {
                plateauDemi[i][j] = new Mur();
            }
        }

        /**
         * Cellule permettant la construction des couloirs lors de son passage
         * sur une case
         */
        class Cellule {

            private Position pos;
            private Direction dir;
            private int coup;

            public Cellule(Position pos, Direction dir, int coup) {
                this.pos = pos;
                this.dir = dir;
                this.coup = coup;
            }

            public boolean hasCoupRestant() {
                coup--;
                return coup >= 0;
            }

            public Position getPos() {
                return pos;
            }

            public Direction getDir() {
                return dir;
            }

            public int getCoup() {
                return coup;
            }

            @Override
            protected Cellule clone() {
                return new Cellule( pos.clone(), dir, coup);
            }

        }

        // liste des positions de couloir
        ArrayList<Position> listeCouloirPosition = new ArrayList();
        //Liste comprenant toutes les cellules en cours de déplacement.
        ArrayList<Cellule> listeCellule = new ArrayList<>();
        //On initialise la première position au coin en haut à gauche avec une durée de vie de 0 afin de provoquer la création
        //d'autres cellules
        listeCellule.add(new Cellule(new Position(1, 1), Direction.AUCUNE, 0));

        do {
            //On copie la liste des cellules afin de pouvoir en ajouter/supprimer sans impacter celle en cours de traitement 
            ArrayList<Cellule> listeTempon = (ArrayList<Cellule>) listeCellule.clone();
            for (Cellule cel : listeTempon) {
                //Si la case est différente d'un couloir, il faut détruire le mur et vérifier si la cellule peut se déplacer
                //On peut le détruire sans vérification de positions car on sait que cela a déjà été fait à sa création
                if (!(plateauDemi[cel.getPos().getX()][cel.getPos().getY()] instanceof Couloir)) {
                    plateauDemi[cel.getPos().getX()][cel.getPos().getY()] = new Couloir();
                    listeCouloirPosition.add( cel.getPos().clone());
                    ((Couloir) plateauDemi[cel.getPos().getX()][cel.getPos().getY()]).setPacGomme(true);
                    //On clone la cellule afin de simuler son déplacement et de vérifier sa validité avant de l'appliquer réellement
                    Cellule celluleClonee =  cel.clone();
                    celluleClonee.getPos().setPositionWithDirection(cel.getDir());
                    //si le mur ne fait pas partie des bordures et que la cellule possède des coups restants, on se déplace
                    if (cel.hasCoupRestant() && celluleClonee.getPos().getX() > 0 && celluleClonee.getPos().getY() > 0
                            && celluleClonee.getPos().getX() < maxLength / 2 && celluleClonee.getPos().getY() < (maxWidth - 1)) {
                        cel.getPos().setPositionWithDirection(cel.dir);
                    } else {
                        //Chaque direction disponible engrange une nouvelle cellule
                        for (Direction direction : Direction.values()) {
                            //On test via un clone la validité de la future cellule avant de la créer
                            Position temp = (Position) cel.getPos().clone();
                            temp.setPositionWithDirection(direction);
                            if ((temp.getX() > 0 && temp.getY() > 0 && temp.getX() < maxLength / 2 && temp.getY() < (maxWidth - 1))
                                    && direction != Tool.dirOpposee(cel.getDir()) && direction != Direction.AUCUNE) {
                                listeCellule.add(new Cellule(temp, direction, Tool.monRandom(longMinCouloir, longMaxCouloir)));
                            }
                        }
                        //on supprime finalement la cellule afin de ne pas réitérer dessus par la suite
                        listeCellule.remove(cel);
                    }
                } else {
                    //on supprime finalement la cellule afin de ne pas réitérer dessus par la suite
                    listeCellule.remove(cel);
                }
            }
        } while (!listeCellule.isEmpty());

        //Pour l'effet de wraparound
        plateauDemi[0][Tool.monRandom(1, maxWidth - 1)] = new Couloir();

        for (int i = 0; i < nbSuperPacgum; i++) {
            Position tempSuperPacgum = listeCouloirPosition.get(Tool.monRandom(0, listeCouloirPosition.size()));
            ((Couloir) plateauDemi[tempSuperPacgum.getX()][tempSuperPacgum.getY()]).setPacGomme(false);
            ((Couloir) plateauDemi[tempSuperPacgum.getX()][tempSuperPacgum.getY()]).setSuperPacGomme(true);
        }

        //Ensuite il faut effectuer le miroir du demi plateau pour obtenir une symétrie
        Case[][] plateauFinal = new Case[maxLength][maxWidth];
        for (int i = 0; i < maxLength / 2; i++) {
            for (int j = 0; j < maxWidth; j++) {
                plateauFinal[i][j] = plateauDemi[i][j];
                if (plateauDemi[i][j] instanceof Mur) {
                    plateauFinal[(maxLength - 1) - i][j] = new Mur();
                } else {
                    plateauFinal[(maxLength - 1) - i][j] = new Couloir();
                    ((Couloir) plateauFinal[(maxLength - 1) - i][j]).setPacGomme(((Couloir) plateauDemi[i][j]).isPacGomme());
                    ((Couloir) plateauFinal[(maxLength - 1) - i][j]).setSuperPacGomme(((Couloir) plateauDemi[i][j]).isSuperPacGomme());

                }
            }
        }
        return plateauFinal;
    }

    /**
     *
     * @param dir
     * @return La direction opposée à celle donnée en paramètre
     */
    public static Direction dirOpposee(Direction dir) {
        switch (dir) {
            case NORD:
                return Direction.SUD;
            case SUD:
                return Direction.NORD;
            case EST:
                return Direction.OUEST;
            case OUEST:
                return Direction.EST;
            default:
                return Direction.AUCUNE;
        }
    }

    public static Position mirrorPosition(Position p, int width, int height) {
        if (p.getX() < width / 2) {
            if (p.getY() < height / 2) {
                return new Position(width - 2, height - 2);
            } else {
                return new Position(width - 2, 1);
            }
        } else {
            if (p.getY() < height / 2) {
                return new Position(1, height - 2);
            } else {
                return new Position(1, 1);
            }
        }
    }
}
