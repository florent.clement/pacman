/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacman;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import launcher.LauncherScene;
import modele.Direction;
import modele.Jeu;

/**
 *
 * @author Florent
 */
public class Pacman extends Application implements EventHandler<KeyEvent> {

    private Stage primaryStage;
    private Jeu jeu;

    public Pacman() {
        this.jeu = new Jeu();
    }

    private void startLaucher() {
        GridPane gPane = new GridPane();
        this.primaryStage.setScene(new LauncherScene(gPane, this));
        this.primaryStage.show();
    }

    public void startGame(Stage primaryStage) {
        primaryStage.hide();
        jeu.initialiser();

        BorderPane border = new BorderPane();
        border.setTop(new GridPane());
        ((GridPane) border.getTop()).setHgap(20);
        border.setBottom(new GridPane());

        ScenePacman scene = new ScenePacman(border, jeu);
        scene.setOnKeyPressed(this);
        jeu.addObserver(scene);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        //Enlève la bar de titre des titres Java
        this.primaryStage.initStyle(StageStyle.UNDECORATED);
        this.primaryStage.setOnCloseRequest(e -> {
            // force l'arrêt du programme + de tout les threads associés
            System.exit(0);
        });
        primaryStage.setTitle("PacMan - TP");
        this.startLaucher();
        //this.startGame(primaryStage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(KeyEvent event) {
        switch (event.getCode()) {
            case F11:
                this.primaryStage.setFullScreen(true);
                break;
            case LEFT:
            case Q:
                jeu.getPacman().setDirection(Direction.OUEST);
                break;
            case RIGHT:
            case D:
                jeu.getPacman().setDirection(Direction.EST);
                break;
            case UP:
            case Z:
                jeu.getPacman().setDirection(Direction.NORD);
                break;
            case DOWN:
            case S:
                jeu.getPacman().setDirection(Direction.SUD);
                break;
            case F4:
                // force l'arrêt du programme + de tout les threads associés
                System.exit(0);
                break;
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Jeu getJeu() {
        return jeu;
    }
}
